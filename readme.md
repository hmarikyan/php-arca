ARMENIAN CARD PAYMENT SYSTEM CLIENT LIBRARY


INSTALLATION

    composer.json
    "repositories": [
        {
            "type":"vcs",
            "url":"https://bitbucket.org/hmarikyan/php-arca.git"
        }
    ],
    "require": {
        "hmarikyan/php-arca": "*"
    },
    
    
    "composer.phar update"


EXAMPLE USAGE:
    
CONFIG 
  
    $config = [
        'test'=>[
            'URL_TOKEN' => "https://91.199.226.7:7002/svpg/Merchant2Rbs",
            'URL_CHARGE' => "https://91.199.226.7:7002/svpg/BPC/AcceptPayment.jsp",
            'URL_CHECK' => "https://91.199.226.7:7002/svpg/QueryOrders",
            'BACKURL'=> "http://localhost/php-arca/check_payment.php",
            'MERCHANTNUMBER' => "118600118603000118604",
            'LANGUAGE' => "EN",
    
            /*
             * 1: 1 level charge
             * 0: 2 level charge
             */
            'DEPOSITFLAG' => '1',
    
            /*first request(verify) - answer mode :
              html/xml/text
            */
            'MODE' => 2,
            'LOGIN' => "merchant",
            'MERCHANTPASSWD' => "lazY2k",
        ],
        'live'=>[
            'URL_TOKEN' => "https://epay.arca.am/svpg/Merchant2Rbs",
            'URL_CHARGE' => "https://91.199.226.7:7002/svpg/BPC/AcceptPayment_123456.jsp",
            'URL_CHECK' => "https://epay.arca.am/svpg/QueryOrders",
            'BACKURL'=> "http://site.com/donate.index.php",
            'MERCHANTNUMBER' => "123456",
            'LANGUAGE' => "EN",
            'DEPOSITFLAG' => '1',
            'MODE' => 2,
            'LOGIN' => "LOG",
            'MERCHANTPASSWD' => "PWD",
    
            /*LIVE CONFIG*/
            'SHOP_TITLE'=> 'title',
            'PATH_NAME'=> 'path',
        ]
    ];


VERIFY AND ORDER PAYMENT

    require_once 'vendor/autoload.php';
    require_once 'config.php';
    
    use Arca\Arca;
    
    $arca_conf = new \Arca\Config($config);
    $arca_conf->set_test();
    
    $arca = new Arca($arca_conf);
    $token = $arca->get_token([
        'AMOUNT'=>100,
        "\$ORDERDESCRIPTION" => "DONATION",
        "ORDERNUMBER" => 124
    ]);
    
    if(!$token){
        echo $arca->get_error();
    }else{
        $arca->redirect();
    }



CHECK PAYMENT AND GET RESPONSE DATA
    
     require_once 'vendor/autoload.php';
     require_once 'config.php';
     
     use Arca\Arca;
     
     $arca_conf = new \Arca\Config($config);
     $arca_conf->set_test();
     
     $arca = new Arca($arca_conf);
     $response = $arca->check_payment($_GET);
     
     if($arca->get_error()){
         echo $arca->get_error();
     }elseif($arca->payment_status()){
         print_r($response);
     }else{
         print_r($response);
     }
       
