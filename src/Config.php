<?php

namespace Arca;

/**
 * Class Config
 * @package Arca
 */
class Config {

    /**
     * @var
     *
     * production data
     */
    protected $live;


    /**
     * @var
     *
     *testing api data
     */
    protected $test;


    /**
     * @var int
     *
     * test: mode = 0
     * live: mode = 1
     */
    protected $mode = 0;


    public function __construct($config = array()){
        if(!empty($config['test'])){
            $this->test = $config['test'];
        }

        if(!empty($config['live'])){
            $this->live = $config['live'];
        }
    }

    /**
     * SETTER
     * Go to live mode
     */
    public function set_live(){
        $this->mode = 1;
    }

    /**
     * SETTER
     * Go to test mode
     */
    public function set_test(){
        $this->mode = 0;
    }


    /**
     * GETTER
     */
    public function get_mode(){
        return $this->mode;
    }

    /**
     * GETTER
     *
     * @return mixed
     */
    public function get_config(){
        return ($this->get_mode() == 1) ? $this->live :$this->test;
    }
}