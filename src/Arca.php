<?php

namespace Arca;

use Curl\Curl;

use Arca\Config;

/**
 * Class Arca
 * @package Arca
 */
class Arca {

    /**
     * @var
     */
    protected $config;

    /**
     * @var null
     */
    protected $error = null;

    /**
     * Payment checked
     *
     * @var null
     */
    protected $checked = null;

    /**
     * @param Config $config
     */
    public function __construct(Config $config){
        $this->config = $config->get_config();

        error_reporting(E_ERROR | E_PARSE);
    }

    /**
     * @param array $params
     * @return bool
     */
    public function get_token($params = array()){

        try{
            if(empty($params['ORDERNUMBER']) || empty($params['AMOUNT']) || empty($params['$ORDERDESCRIPTION']))
                throw new \Exception("ERROR: Missing either ORDERNUMBER or AMOUNT or \$ORDERDESCRIPTION");

            /*prepare request data*/
            $request_data = array(
                 'MERCHANTNUMBER'=> $this->config['MERCHANTNUMBER'],
                 'ORDERNUMBER'=>$params['ORDERNUMBER'],
                 'AMOUNT'=>$params['AMOUNT'],
                 'BACKURL'=> $this->config['BACKURL'],
                 '$ORDERDESCRIPTION'=>$params['$ORDERDESCRIPTION'],
                 'LANGUAGE'=>$this->config['LANGUAGE'],
                 'DEPOSITFLAG'=>$this->config['DEPOSITFLAG'],
                 'MERCHANTPASSWD'=>$this->config['MERCHANTPASSWD'],
                 'DEPOSITFLAG'=>$this->config['DEPOSITFLAG'],
                 'MODE'=>$this->config['MODE'],
            );

            /*make request and get verify token*/
            $request = $this->config['URL_TOKEN'] . (empty($request_data) ? '' : '?' . http_build_query($request_data));
            $response = file_get_contents($request);

            /*obtain token from response*/
            if($token = $this->parse_token($response)){
                $this->token = $token;
                return $token;
            }else{
                throw new \Exception("ERROR: $response");
            }


        }catch(\Exception $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    /**
     * REDIRECT TO ARCA SITE
     */
    public function redirect(){

        /*prepare request data*/
        $request_data = array(
            'MDORDER' => $this->token,
            'LOGIN' => $this->config['LOGIN']
        );

        /*if live and set config for styles*/
        if(!empty($this->config['SHOP_TITLE']) || !empty($this->config['PATH_NAME'])){
            $request_data['title'] = $this->config['SHOP_TITLE'];
            $request_data['path'] = $this->config['PATH_NAME'];
        }

        $request = $this->config['URL_CHARGE'] . (empty($request_data) ? '' : '?' . http_build_query($request_data));

        header("LOCATION: ". $request);
    }

    /**
     * CHECK
     *
     * @param $post
     */
    public function check_payment($get){

        try{
            if(empty($get['MDORDER']))
               throw new \Exception("ERROR: Missing MDORDER field");

            /*prepare request data*/
            $request_data = array(
                'MDORDER' => $get['MDORDER'],
                'MERCHANTPASSWD' => $this->config['MERCHANTPASSWD']
            );


            $request = $this->config['URL_CHECK'] . (empty($request_data) ? '' : '?' . http_build_query($request_data));
            $response = file_get_contents($request);

            $response = $this->parse_response($response);

            if(!empty($response['payment_state']) && $response['payment_state'] == "payment_approved") {
                $this->checked = 1;
            }

            return $response;


        }catch(\Exception $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    /**
     *
     * payment is succeed
     * @return bool
     */
    public function payment_status(){
        if($this->checked == 1)
            return true;
        else
            return false;

    }

    /**
     * @param $response
     */
    public function parse_token($response){

       try{
           $doc = new \DOMDocument();
           $doc->loadXML($response);

           $items = $doc->getElementsByTagName('mdorder');

           $token = null;
           if(!empty($items) > 0)
           {
               foreach($items as $item){
                   $token = $item->nodeValue;
                   break;
               }

               return $token;
           }else
               throw new \Exception();
       }catch (\Exception $e){
           return false;
       }

    }

    /**
     *
     */
    public function parse_response($response){
        $xml = simplexml_load_string($response);

        $return_data = array();

        //print_r($xml);

        $ps_order = $xml->PSOrder->attributes();
        if(empty($ps_order))
            throw new \Exception("ERROR: Response is not correct");
        foreach($ps_order as $key=>$value)
        {
            $return_data[$key] = (string)$value;
        }

        //print_r($xml->PSOrder->PaymentCollection->PSPayment->attributes());

        $ps_payment = $xml->PSOrder->PaymentCollection->PSPayment->attributes();
        if(empty($ps_payment))
            throw new \Exception("ERROR: Response is not correct");
        foreach($ps_payment as $key=>$value)
        {
            $return_data[$key] = (string)$value;
        }

        return $return_data;
    }

    /**
     * GETTER
     *
     * @return null
     */
    public function get_error(){
        return $this->error;
    }
} 