<?php
require_once 'vendor/autoload.php';
require_once 'config.php';

use Arca\Arca;

$arca_conf = new \Arca\Config($config);
$arca_conf->set_test();

$arca = new Arca($arca_conf);
$token = $arca->get_token([
    'AMOUNT'=>100,
    "\$ORDERDESCRIPTION" => "DONATION",
    "ORDERNUMBER" => 164
]);

if(!$token){
    echo $arca->get_error();
}else{
    $arca->redirect();
}
