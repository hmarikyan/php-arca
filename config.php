<?php

$config = [
    'test'=>[
        'URL_TOKEN' => "https://91.199.226.7:7002/svpg/Merchant2Rbs",
        'URL_CHARGE' => "https://91.199.226.7:7002/svpg/BPC/AcceptPayment.jsp",
        'URL_CHECK' => "https://91.199.226.7:7002/svpg/QueryOrders",
        'BACKURL'=> "http://localhost/php-arca/check_payment.php",
        'MERCHANTNUMBER' => "118600118603000118604",
        'LANGUAGE' => "EN",

        /*
         * 1: 1 level charge
         * 0: 2 level charge
         */
        'DEPOSITFLAG' => '1',

        /*first request(verify) - answer mode :
          html/xml/text
        */
        'MODE' => 2,
        'LOGIN' => "merchant",
        'MERCHANTPASSWD' => "lazY2k",
    ],
    'live'=>[
        'URL_TOKEN' => "https://epay.arca.am/svpg/Merchant2Rbs",
        'URL_CHARGE' => "https://91.199.226.7:7002/svpg/BPC/AcceptPayment_123456789.jsp",
        'URL_CHECK' => "https://epay.arca.am/svpg/QueryOrders",
        'BACKURL'=> "http://impoqrik.am/donate.index.php",
        'MERCHANTNUMBER' => "",
        'LANGUAGE' => "EN",
        'DEPOSITFLAG' => '1',
        'MODE' => 2,
        'LOGIN' => "",
        'MERCHANTPASSWD' => "",

        /*LIVE CONFIG*/
        'SHOP_TITLE'=> '',
        'PATH_NAME'=> '',
    ]
];

return $config;