<?php
require_once 'vendor/autoload.php';
require_once 'config.php';

use Arca\Arca;

$arca_conf = new \Arca\Config($config);
$arca_conf->set_test();

$arca = new Arca($arca_conf);
$response = $arca->check_payment($_GET);

if($arca->get_error()){
    echo $arca->get_error();
}elseif($arca->payment_status()){
    print_r($response);
}else{
    print_r($response);
}
